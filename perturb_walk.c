#include "linear.h"
#include "echo.h"
#include "echo_util.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

// TODO: Change this to take a distribution and add it to util.
void perturb_additative_dense( dense_matrix* matrix
			     , double sd, gsl_rng* gen
			     , cholmod_common* chol ) {
    const unsigned int range = matrix->nzmax;
    double* ptr = (double*)matrix->x;

    for(int i = 0; i < range; ++i)
	ptr[i] += gsl_ran_gaussian(gen, sd);
}

void perturb_additative_sparse( sparse_matrix* matrix
			      , double sd, gsl_rng* gen
			      , cholmod_common* chol ) {
    const unsigned int range = matrix->nzmax;
    double* ptr = (double*)matrix->x;

    for(int i = 0; i < range; ++i)
	ptr[i] += gsl_ran_gaussian(gen, sd);
}

void ensure_esp_sparse( sparse_matrix* matrix
		      , double spec
		      , gsl_rng* gen
		      , cholmod_common* chol ) {
    dense_vector* eigenvector =
	linear_dense_vector_random(matrix->nrow, gen, &dense_dist, (void*)0, chol);

    double eigenvalue = 
	power_iteration_sparse(matrix, eigenvector, chol);

    if (eigenvalue > 1){
	linear_unsafe_sparse_matrix_scale(spec/eigenvalue, matrix, chol);
    }

    linear_dense_vector_free(eigenvector, chol);
}

int main(int argc, char** argv) {
    if(argc < 7) {
	printf("Usage: %s input target test_input test_target connectivity reservoir_size (lag) (replicates)\n", argv[0]);
	exit(EXIT_FAILURE);
    }

    cholmod_common chol;
    cholmod_start(&chol);

    gsl_rng *gen = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set (gen, time(NULL));

    const unsigned int reservoir_size = atoi(argv[6]);

    const unsigned int input_dim  = 1;
    const unsigned int output_dim = 1;

    const unsigned int lag = 
	argc < 8 ? 10 : atoi(argv[7]);

    const unsigned int replicates = 
	argc < 9 ? 100 : atoi(argv[8]);

    FILE* f_input = fopen(argv[1], "r");    
    if(!f_input) {
	printf("Unable to read input file: %s\n", argv[1]);
	exit(EXIT_FAILURE);
    }
    echo_series* input = 
	echo_util_read_doubles(f_input, &chol);
    fclose(f_input);

    FILE* f_target = fopen(argv[2], "r");    
    if(!f_target) {
	printf("Unable to read target file: %s\n", argv[2]);
	exit(EXIT_FAILURE);
    }
    echo_series* target_data =
	echo_util_read_doubles(f_target, &chol);
    fclose(f_target);

    FILE* f_test_input = fopen(argv[3], "r");    
    if(!f_test_input) {
	printf("Unable to read test_input file: %s\n", argv[3]);
	exit(EXIT_FAILURE);
    }
    echo_series* test_input =
	echo_util_read_doubles(f_test_input, &chol);
    fclose(f_test_input);

    FILE* f_test_target = fopen(argv[4], "r");    
    if(!f_test_target) {
	printf("Unable to read test_target file: %s\n", argv[4]);
	exit(EXIT_FAILURE);
    }
    echo_series* test_target_data =
	echo_util_read_doubles(f_test_target, &chol);
    fclose(f_test_target);

    double* train_mse = malloc(sizeof(double)*replicates);
    double* test_mse  = malloc(sizeof(double)*replicates);

    echo_network net = 
	echo_util_make_network( reservoir_size, input_dim, output_dim
			      , gen, &chol );

    echo_series *target = echo_util_output_series_for(net, target_data->nrow - lag, &chol);
    memcpy(target->x, ((double*)target_data->x) + lag, (target->nrow)*sizeof(double));

    echo_series *test_target = echo_util_output_series_for(net, test_target_data->nrow - lag, &chol);
    memcpy(test_target->x, ((double*)test_target_data->x) + lag, (test_target->nrow)*sizeof(double));

    double mse;
    double residual;
    double* ptr_run;
    double* ptr_target;
    echo_series* run;
    const unsigned int run_length = test_target->nrow;

    time_t* at_times = malloc(sizeof(time_t)*replicates);
    //TODO: Should probably use clock_gettime, but somehow I'm too dumb
    time_t base_time = clock();
    for(int i = 0; i < replicates; ++i) {
	echo_util_train_network(net, lag, input, target, gen, &chol);

	//TODO: Write an unsafe version, so that we can reuse run!
	run = echo_util_eval_network_all(net, lag, input, gen, &chol);

	// TODO: Extract this and other useful statistical procedures
	// into a separte thing!
	mse = 0;
	ptr_run    = (double*)run->x;
	ptr_target = (double*)target->x;
	for(int j = 0; j < target -> nrow; ++j) {
	    residual = ptr_run[j] - ptr_target[j];
	    mse += residual*residual;
	} 
	train_mse[i] = mse/target -> nrow;

	echo_util_series_free(run, &chol);

	run = echo_util_eval_network_all(net, lag, test_input, gen, &chol);

	mse = 0;
	ptr_run    = (double*)run->x;
	ptr_target = (double*)test_target->x;
	for(int j = 0; j < run_length; ++j) {
	    residual = ptr_run[j] - ptr_target[j];
	    mse += residual*residual;
	} 
	test_mse[i] = mse/run_length;
	fprintf(stderr, "%f\n", mse);

	echo_util_series_free(run, &chol);

	at_times[i] = clock() - base_time;

	perturb_additative_sparse(net.dynamics, 0.1, gen, &chol);
	ensure_esp_sparse(net.dynamics, 0.95, gen, &chol);
	perturb_additative_dense(net.readin, 0.1, gen, &chol);
    }

    printf("Test MSE\tTrain MSE\tTime\n");
    for(int i = 0; i < replicates; ++i)
	printf( "%f\t%f\t%Lf\n"
	      , test_mse[i]
	      , train_mse[i]
		// Lol, I wanted to be absolutely sure...
	      , ((long double)at_times[i])/((long double)CLOCKS_PER_SEC));

    free(train_mse);
    free(test_mse);

    echo_util_net_free(net, &chol);
    echo_util_series_free(input, &chol);
    echo_util_series_free(target, &chol);
    echo_util_series_free(target_data, &chol);
    echo_util_series_free(test_input, &chol);
    echo_util_series_free(test_target, &chol);
    echo_util_series_free(test_target_data, &chol);

    cholmod_finish(&chol);

    gsl_rng_free(gen);

    return EXIT_SUCCESS;
}
