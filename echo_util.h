/*********************************************************************************/
/* The MIT License (MIT)                                                         */
/*                                                                               */
/* Copyright (c) 2013 Tilo Wiklund, Ross Linscott                                */
/*                                                                               */
/* Permission is hereby granted, free of charge, to any person obtaining a copy  */
/* of this software and associated documentation files (the "Software"), to deal */
/* in the Software without restriction, including without limitation the rights  */
/* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell     */
/* copies of the Software, and to permit persons to whom the Software is         */
/* furnished to do so, subject to the following conditions:                      */
/*                                                                               */
/* The above copyright notice and this permission notice shall be included in    */
/* all copies or substantial portions of the Software.                           */
/*                                                                               */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR    */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,      */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE   */
/* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER        */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, */
/* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN     */
/* THE SOFTWARE.                                                                 */
/*********************************************************************************/

#ifndef ECHO_UTIL_H
#define ECHO_UTIL_H

#include "linear.h"
#include "echo.h"

double sparse_dist(const void* null, const gsl_rng* gen);
double sparse_dist_nondumb(const void* param, const gsl_rng* gen);
double dense_dist(const void* null, const gsl_rng* gen);

double esdyn_dist(const void *null, const gsl_rng *gen);

double norm(cholmod_dense* vec, cholmod_common* chol);

double normalise(cholmod_dense* vec, cholmod_common* chol);

void swap(void** a, void** b);

void swap_dense(cholmod_dense* a, cholmod_dense* b);

void fbumb(double* a);

void unsafe_twiddle( dense_matrix* dense_dynamics
		   , gsl_rng* gen
		     , cholmod_common* chol );

double power_iteration_dense( cholmod_dense* matrix, cholmod_dense* eigen
			      , cholmod_common* chol );

double power_iteration_sparse( cholmod_sparse* matrix, cholmod_dense* eigen
			       , cholmod_common* chol );

echo_series* echo_util_series_alloc( const unsigned int entries 
				   , const unsigned int dimension
				   , cholmod_common* chol );

void echo_util_series_free(echo_series* series, cholmod_common* chol);

echo_network echo_util_make_network( const unsigned int reservoir_size
				    , const unsigned int input_dim
				    , const unsigned int output_dim
			            , gsl_rng* gen
				    , cholmod_common* chol );

void echo_util_net_free(echo_network net, cholmod_common* chol);

echo_state* echo_util_make_state_for(echo_network net, gsl_rng* gen, cholmod_common* chol);

void echo_util_state_free(echo_state* state, cholmod_common* chol);

sparse_matrix* echo_util_generate_trimmed_dynamics(int reservoir_size
						   , gsl_rng *gen
						   , cholmod_common *chol);

sparse_matrix* echo_util_generate_es_dynamics( int reservoir_size
					     , double srad
					     , gsl_rng *gen
					     , cholmod_common *chol );

sparse_matrix* echo_util_generate_es_dynamics_with( int reservoir_size
					          , double srad
						  , distribution dist
						  , const void* dist_param
					          , gsl_rng *gen
						  , cholmod_common *chol );

void echo_util_train_network( echo_network net
		            , const unsigned int lag
		            , echo_series* input
		            , echo_series* target
		            , gsl_rng* gen
			    , cholmod_common* chol);

echo_series* echo_util_online_train( echo_network net
				     , echo_series* input
				     , echo_series* target
				     , echo_series* out_output
				     , double eta
				     , double decay
				     , gsl_rng* gen
				     , cholmod_common* chol);

echo_series* echo_util_output_series_for( const echo_network net
				        , const unsigned int entries 
					, cholmod_common* chol );

echo_series* echo_util_input_series_for( const echo_network net
				       , const unsigned int entries 
				       , cholmod_common* chol );

echo_series* echo_util_eval_network( echo_network net
				   , const unsigned int lag
				   , const unsigned int num
				   , echo_series* input
				   , gsl_rng* gen
				   , cholmod_common* chol );

echo_series* echo_util_eval_network_all( echo_network net
				       , const unsigned int lag
				       , echo_series* input
				       , gsl_rng* gen
				       , cholmod_common* chol );

echo_series* echo_util_piecewise_prediction( echo_network net
					     , const unsigned int run_steps
					     , const unsigned int lookahead
					     , echo_series* input
					     , gsl_rng* gen
					     , cholmod_common* chol );

void echo_util_serialise_dense_matrix(dense_matrix* matrix, FILE* stream, cholmod_common* chol);
void echo_util_serialise_dense_vector(dense_vector* vector, FILE* stream, cholmod_common* chol);
void echo_util_serialise_sparse_matrix(sparse_matrix* matrix, FILE* stream, cholmod_common* chol);
void echo_util_serialise_dynamics(echo_dynamics* dynamics, FILE* stream, cholmod_common* chol);
void echo_util_serialise_readin(echo_readin* dynamics, FILE* stream, cholmod_common* chol);
void echo_util_serialise_readout(echo_readout* dynamics, FILE* stream, cholmod_common* chol);
void echo_util_serialise_network(echo_network net, FILE* stream, cholmod_common* chol);

dense_matrix*  echo_util_deserialise_dense_matrix(FILE* stream, cholmod_common* chol);
dense_vector*  echo_util_deserialise_dense_vector(FILE* stream, cholmod_common* chol);
sparse_matrix* echo_util_deserialise_sparse_matrix(FILE* stream, cholmod_common* chol);
echo_dynamics* echo_util_deserialise_dynamics(FILE* stream, cholmod_common* chol);
echo_readin*   echo_util_deserialise_readin(FILE* stream, cholmod_common* chol);
echo_readout*  echo_util_deserialise_readout(FILE* stream, cholmod_common* chol);
echo_network   echo_util_deserialise_network(FILE* stream, cholmod_common* chol);

echo_series* echo_util_read_doubles(FILE* stream, cholmod_common* chol);
echo_series* echo_util_read_doubles_lag(FILE* stream, const unsigned int lag, cholmod_common* chol);
void echo_util_write_doubles(FILE* stream, echo_series *data, cholmod_common* chol);

#endif
