#include <time.h>
#include <string.h>
#include <gsl/gsl_rng.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "echo_util.h"
#include "echo_series.h"
#include "pso.h"

typedef struct {
    echo_network net;
    int size;
    int lag;
    double epsilon;
    double range;

    echo_series *training_input;
    echo_series *training_target;
    echo_series *test_input;
    echo_series *test_target;

    dense_matrix *build_buf;

    int n_train;
    int n_test;

    time_t t_start;
    char *fname;
    gsl_rng* gen;
    cholmod_common *chol;
} net_conf; 

echo_dynamics *build_dynamics(double *x, net_conf *c){
    int max = c -> size * c -> size;
    int i;
    double *p;
    double scale = c -> range / (c -> range - c -> epsilon);

    p = c -> build_buf -> x;

    for (i = 0; i < max; i++){
	p[i] = scale * ((x[i] > 0) - (x[i] < 0)) * (fabs(x[i]) - c -> epsilon) * (fabs(x[i]) > c -> epsilon);
    }

    return cholmod_dense_to_sparse(c -> build_buf, 1, c -> chol);
}

// TODO: convert x to a dense matrix, remove (small?) elements and make a sparse matrix to use as dynamics
double error_measure(double *x, int dim, void *params){
    net_conf *c = params;

    gsl_rng_set (c -> gen, 48573);

    c -> net.dynamics = build_dynamics(x, c);
    c -> net.readin -> x = x + c -> size * c -> size;
  
    echo_util_train_network(c -> net, c -> lag, c -> training_input, c -> training_target, c -> gen, c -> chol);

    echo_series *result = echo_util_eval_network(c -> net, c -> lag, c -> n_test, c -> test_input, c -> gen, c -> chol);
    double ret = linear_dense_distance2(result, c -> test_target) / (c -> n_test - c -> lag);

    echo_util_series_free(result, c -> chol);
    linear_sparse_matrix_free(c -> net.dynamics, c -> chol);
    return ret;
}

void record_result(double *x, int dim, void *params, double err){
    net_conf *c = params;
    time_t t_current = clock();
    double time_diff = (t_current - c -> t_start) / (double)CLOCKS_PER_SEC;
    double srad;
    dense_vector *eig;

    gsl_rng_set (c -> gen, 48573);

    c -> net.dynamics = build_dynamics(x, c);
    c -> net.readin -> x = x + c -> size * c -> size;

    eig = linear_dense_vector_random(c -> net.dynamics -> nrow, c -> gen, dense_dist, (void*)0, c -> chol);
    srad = power_iteration_sparse(c -> net.dynamics, eig, c -> chol);
    linear_dense_vector_free(eig, c -> chol);
  
    echo_util_train_network(c -> net, c -> lag, c -> training_input, c -> training_target, c -> gen, c -> chol);

    FILE *file = fopen(c -> fname, "w");
    echo_util_serialise_network(c -> net, file, c -> chol);
    fclose(file);
  
    linear_sparse_matrix_free(c -> net.dynamics, c -> chol);

    fprintf(stdout,"%f\t%f\t%f\n", time_diff, srad, err);
}

int main(int argc, char **argv){
    pso_settings_t settings;
    pso_result_t solution;
    cholmod_common chol;
    net_conf conf;
    gsl_rng *gen = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng *pso_rng = gsl_rng_alloc(gsl_rng_mt19937);
    int i;

    cholmod_start(&chol);

    gsl_rng_set(pso_rng, time(NULL));

    if (argc < 3 || !strcmp(argv[1], "--help")){
	fprintf(stdout,"usage: %s [options] training_input training_target [testing_input testing_target]\n", argv[0]);
	fprintf(stdout,"Echo PSO uses particle swarm optimisation to search for the optimal dynamics of an ESN with respect to the data.\n");
	fprintf(stdout,"At each iteration, the step, time, inertia and error are printed to stdout.\n");
	fprintf(stdout,"If no testing data are supplied, the last 20%% of the input data are used for testing.\n");
	fprintf(stdout,"The resulting network is stored in tmp_pso.esn. The readout of the network can be retrained for better performance with the train_network command.\n");
	fprintf(stdout,"Available options are:\n");
	fprintf(stdout,"-rsize N: set the reservoir size to N nodes (default:100).\n");
	fprintf(stdout,"-wrange W: set the maximum weight size for the dynamics (default: 1).\n");
	fprintf(stdout,"-popsize N: set the PSO pop size (default: 10).\n");
	fprintf(stdout,"-steps N: set the maximum number of steps for the PSO (defaul: 100).\n");
	fprintf(stdout,"-lag N: set the lag (defaul: 100; numel(train_input) - lag must be >= rsize + 1 + d_in).\n");
	fprintf(stdout,"-o fname: set the output file name (default: tmp_pso.esn).\n");
	return 0;
    }

    // load defaul values
    int reservoir_size = 100;
    int d_in = 1;
    int d_out = 1;
    double range = 1;
    int pso_pop = 10;
    int pso_steps = 100;
    conf.fname = "pso_tmp.esn";
    conf.lag = 100;

    conf.gen = gen;
    conf.chol = &chol;

    // load options
    for (i = 1; i < argc; i++){
	if (!strcmp(argv[i], "-rsize")){
	    i++;
	    reservoir_size = atoi(argv[i]);
	}else if (!strcmp(argv[i], "-wrange")){
	    i++;
	    range = atof(argv[i]);
	}else if (!strcmp(argv[i], "-popsize")){
	    i++;
	    pso_pop = atoi(argv[i]);
	}else if (!strcmp(argv[i], "-steps")){
	    i++;
	    pso_steps = atoi(argv[i]);
	}else if (!strcmp(argv[i], "-lag")){
	    i++;
	    conf.lag = atoi(argv[i]);
	}else if (!strcmp(argv[i], "-o")){
	    i++;
	    conf.fname = argv[i];
	}else if (argv[i][0] == '-'){
	    fprintf(stderr,"Bad option: %s. Run with --help for usage.\n", argv[i]);
	    exit(EXIT_FAILURE);
	}else{
	    break;
	}
    }

    if (argc - i < 2){
	fprintf(stderr, "Missing data file arguments! Run with --help for usage.\n");
	return -1;
    }

    FILE *f_input = fopen(argv[i], "r");
    echo_series *input_data = echo_util_read_doubles(f_input, &chol);
    fclose(f_input);
    FILE *f_target = fopen(argv[i+1], "r");
    echo_series *target_data = echo_util_read_doubles_lag(f_target, conf.lag, &chol);
    fclose(f_target);

    // make network without dynamics or readin (as these are tweaked by the PSO)
    conf.net = echo_util_make_network(reservoir_size, d_in, d_out, conf.gen, conf.chol);
    linear_sparse_matrix_free(conf.net.dynamics, &chol);
    free(conf.net.readin -> x);
  
    conf.epsilon = 0.8 * range;
    conf.range = range;
    conf.size = reservoir_size;
    conf.build_buf = linear_dense_matrix_alloc(reservoir_size, reservoir_size, &chol);

    // read test data, or split train data, depending on parameters
    if (argc - i == 4){
	conf.training_input = input_data;
	conf.training_target = target_data;

	f_input = fopen(argv[i+2], "r");
	conf.test_input = echo_util_read_doubles(f_input, &chol);
	fclose(f_input);

	f_target = fopen(argv[i+3], "r");
	conf.test_target = echo_util_read_doubles_lag(f_target, conf.lag, &chol);
	fclose(f_target);

	conf.n_train = conf.training_input -> nrow;
	conf.n_test = conf.test_input -> nrow;
    }else if (argc - i == 2){
	// setup training and test data by splitting loaded series

	int num_entries = input_data -> nrow;
	conf.n_train = 0.8 * num_entries;
	conf.n_test = num_entries - conf.n_train;

	conf.training_input = echo_util_input_series_for(conf.net, conf.n_train, &chol);
	conf.training_target = echo_util_output_series_for(conf.net, conf.n_train - conf.lag, &chol);
	conf.test_input = echo_util_input_series_for(conf.net, conf.n_test, &chol);
	conf.test_target = echo_util_output_series_for(conf.net, conf.n_test - conf.lag, &chol);

	memcpy(conf.training_input -> x, input_data -> x, conf.n_train * sizeof(double));
	memcpy(conf.training_target -> x, ((double*)target_data -> x) + conf.lag, (conf.n_train - conf.lag) * sizeof(double));
	memcpy(conf.test_input -> x, ((double*)input_data -> x) + conf.n_train, conf.n_test * sizeof(double));
	memcpy(conf.test_target -> x, ((double*)target_data -> x) + conf.n_train + conf.lag, (conf.n_test - conf.lag) * sizeof(double));

	echo_util_series_free(input_data, &chol);
	echo_util_series_free(target_data, &chol);
    }else{
	fprintf(stderr,"Error: bad number of data file arguments (got %d, expected 2 or 4)! Run with --help for usage.\n", argc - i);
	exit(EXIT_FAILURE);
    }
    
    pso_set_default_settings(&settings);

    //tweak pso settings
    settings.dim = reservoir_size * (reservoir_size + d_in);
    settings.size = pso_pop;
    settings.w_strategy = PSO_W_LIN_DEC;
    settings.x_lo = -range;
    settings.x_hi = range;
    settings.goal = 0;
    settings.nhood_strategy = PSO_NHOOD_RING;
    settings.nhood_size = settings.size / 4;
    settings.print_every = 1;
    settings.steps = pso_steps;
    settings.rng = pso_rng;
  
    if (conf.n_test < 1 || conf.n_train < 1){
	fprintf(stderr, "insufficient data (required 2, got %d)\n", conf.test_input -> nrow + conf.training_input -> nrow);
	exit(EXIT_FAILURE);
    }

    if (conf.n_train - conf.lag < reservoir_size + d_in + 1){
	fprintf(stderr, "insufficient data after lag (required %d, got %d)\n", reservoir_size + d_in + 1, conf.n_train);
	exit(EXIT_FAILURE);
    }

    //fprintf(stdout, "Setting up data with %d training entries and %d test entries.\n", conf.n_train, conf.n_test);
    solution.gbest = malloc(settings.dim * sizeof(double));

    if (!solution.gbest){
	fprintf(stderr,"failed to alloc gbest!\n");
	exit(EXIT_FAILURE);
    }

    //run the solver

    conf.t_start = clock();
    pso_solve(error_measure, record_result, &conf, &solution, &settings);

    record_result(solution.gbest, settings.dim, &conf, solution.error);

    conf.net.dynamics = NULL;
    conf.net.readin -> x = NULL;
    echo_util_net_free(conf.net, &chol);
    echo_util_series_free(conf.training_input, &chol);
    echo_util_series_free(conf.training_target, &chol);
    echo_util_series_free(conf.test_input, &chol);
    echo_util_series_free(conf.test_target, &chol);

    linear_dense_matrix_free(conf.build_buf, &chol);

    free(solution.gbest);
    cholmod_finish(&chol);
    gsl_rng_free(gen);
    return 0;
}
