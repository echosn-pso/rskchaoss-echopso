#include "linear.h"
#include "echo.h"
#include "echo_util.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

int main(int argc, char** argv) {
    if(argc < 3) {
	printf("Usage: %s network input (lag)\n", argv[0]);
	exit(EXIT_FAILURE);
    }

    cholmod_common chol;
    cholmod_start(&chol);

    gsl_rng *gen = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set (gen, time(NULL));

    const unsigned int lag = 
	argc == 3 ? 5 : atoi(argv[3]);

    FILE* f_input = fopen(argv[2], "r");    
    if(!f_input) {
	printf("Unable to read input file: %s\n", argv[1]);
	exit(EXIT_FAILURE);
    }
    echo_series* input = 
	echo_util_read_doubles(f_input, &chol);
    fclose(f_input);

    FILE* f_network = fopen(argv[1], "r");    
    echo_network net = echo_util_deserialise_network(f_network, &chol);
    fclose(f_network);

    echo_series* run = 
	echo_util_eval_network_all(net, lag, input, gen, &chol);

    const unsigned int range = run->nrow;
    double* x = (double*)run->x;
    for(int i = 0; i < range; ++i) 
	printf("%f\n", x[i]);

    echo_util_net_free(net, &chol);
    echo_util_series_free(input, &chol);
    echo_util_series_free(run, &chol);

    cholmod_finish(&chol);

    gsl_rng_free(gen);

    return EXIT_SUCCESS;
}
