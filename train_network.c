#include "linear.h"
#include "echo.h"
#include "echo_util.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

int main(int argc, char** argv) {
    if(argc < 5) {
	printf("Usage: %s network result input target (lag)\n", argv[0]);
	exit(EXIT_FAILURE);
    }

    cholmod_common chol;
    cholmod_start(&chol);

    gsl_rng *gen = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set (gen, time(NULL));

    const unsigned int lag = 
	argc == 5 ? 50 : atoi(argv[5]);

    FILE* f_input = fopen(argv[3], "r");    
    if(!f_input) {
	printf("Unable to read input file: %s\n", argv[1]);
	exit(EXIT_FAILURE);
    }
    echo_series* input = 
	echo_util_read_doubles(f_input, &chol);
    fclose(f_input);

    FILE* f_target = fopen(argv[4], "r");    
    if(!f_input) {
	printf("Unable to read input file: %s\n", argv[1]);
	exit(EXIT_FAILURE);
    }
    echo_series* target_data = 
	echo_util_read_doubles(f_target, &chol);
    fclose(f_input);
    int num_entries = target_data -> nrow;

    FILE* f_network = fopen(argv[1], "r");  
    if (!(f_network)){
	fprintf(stderr,"Unable to read network file: %s\n", argv[1]);
	exit(EXIT_FAILURE);
    }
    echo_network net = echo_util_deserialise_network(f_network, &chol);
    fclose(f_network);

    echo_series *target = echo_util_output_series_for(net, num_entries - lag, &chol);
    memcpy(target -> x, ((double*)target_data -> x) + lag, (num_entries - lag) * sizeof(double));

    echo_util_train_network(net, lag, input, target, gen, &chol);

    f_network = fopen(argv[2], "w");
    echo_util_serialise_network(net, f_network, &chol);
    fclose(f_network);

    echo_util_net_free(net, &chol);
    echo_util_series_free(input, &chol);
    echo_util_series_free(target_data, &chol);
    echo_util_series_free(target, &chol);

    cholmod_finish(&chol);

    gsl_rng_free(gen);

    return EXIT_SUCCESS;
}
