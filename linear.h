/*********************************************************************************/
/* The MIT License (MIT)                                                         */
/*                                                                               */
/* Copyright (c) 2013 Tilo Wiklund, Ross Linscott                                */
/*                                                                               */
/* Permission is hereby granted, free of charge, to any person obtaining a copy  */
/* of this software and associated documentation files (the "Software"), to deal */
/* in the Software without restriction, including without limitation the rights  */
/* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell     */
/* copies of the Software, and to permit persons to whom the Software is         */
/* furnished to do so, subject to the following conditions:                      */
/*                                                                               */
/* The above copyright notice and this permission notice shall be included in    */
/* all copies or substantial portions of the Software.                           */
/*                                                                               */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR    */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,      */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE   */
/* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER        */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, */
/* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN     */
/* THE SOFTWARE.                                                                 */
/*********************************************************************************/

#ifndef ECHO_LINEAR_H
#define ECHO_LINEAR_H

#include <gsl/gsl_rng.h>
#include <cblas.h>
#include <cholmod.h>
#include <clapack.h>

// TODO: Change these to single component structs?
typedef cholmod_sparse sparse_matrix;
typedef cholmod_dense  dense_matrix;
typedef cholmod_dense  dense_vector;

// TODO: the whole package might light some additional const!

// TODO: I think we need some extra work on the naming conventions, and
// how to handle safe/unsafe functions

// TODO: I think we might want to add a procedure for produceing a
// vector/matrix identical to another (in shape/type, not data) as
// well as a procedure for producing a vector of another shape but
// sharing data (esp. for treating columns of a matrix as a vector).

dense_matrix* linear_dense_identity( const unsigned int n, const unsigned int m
				   , cholmod_common* chol );

sparse_matrix* linear_sparse_identity( const unsigned int n, const unsigned int m
				     , cholmod_common* chol );

dense_matrix* linear_dense_matrix_alloc( const unsigned int n, const unsigned int m
				       , cholmod_common* chol);

void linear_dense_matrix_free(dense_matrix* matrix, cholmod_common* chol);

dense_vector* linear_dense_vector_alloc( const unsigned int n
				       , cholmod_common* chol);

void linear_dense_vector_free(dense_vector* vector, cholmod_common* chol);

// See source file note
/* sparse_matrix* linear_sparse_matrix_alloc( const unsigned int n, const unsigned int m */
/* 					 , const unsigned int size */
/* 				         , cholmod_common* chol); */

void linear_sparse_matrix_free(sparse_matrix* matrix, cholmod_common* chol);

void linear_dense_vector_smooth(dense_vector *x, cholmod_common *chol);

void linear_dense_vector_copy_to( dense_vector* to
				, dense_vector* from
				, char expect_memcpy
				, cholmod_common* chol );

void linear_dense_matrix_copy_to( dense_vector* to
				, dense_vector* from
				, char expect_memcpy
				, cholmod_common* chol );

dense_vector* linear_dense_vector_zero(const unsigned int size, cholmod_common* chol);

sparse_matrix* linear_dense_to_sparse( dense_matrix* matrix
				     , cholmod_common* chol );

dense_matrix* linear_sparse_to_dense( sparse_matrix* matrix
				    , cholmod_common* chol );

sparse_matrix* linear_sparse_matrix_scale( const double coef
					 , sparse_matrix* matrix
                                         , cholmod_common* chol );

dense_matrix* linear_dense_matrix_scale( const double coef
				       , dense_matrix* matrix
                                       , cholmod_common* chol );

dense_vector* linear_dense_vector_scale( const double coef
				       , dense_vector* vector
                                       , cholmod_common* chol );

void linear_unsafe_sparse_matrix_scale( const double coef
				      , sparse_matrix* matrix
                                      , cholmod_common* chol );

void linear_unsafe_dense_matrix_scale( const double coef
				     , dense_matrix* matrix
                                     , cholmod_common* chol );

void linear_unsafe_dense_vector_scale( const double coef
				     , dense_vector* vector
                                     , cholmod_common* chol );

sparse_matrix* linear_sparse_matrix_add( sparse_matrix* matrix1
				       , sparse_matrix* matrix2
			               , cholmod_common* chol );

dense_matrix* linear_dense_matrix_add( dense_matrix* matrix1
				     , dense_matrix* matrix2
			             , cholmod_common* chol );

dense_vector* linear_dense_vector_add( dense_vector* vector1, dense_vector* vector2
			             , cholmod_common* chol );

//TODO: Add something to indicate that the first matrix is the one mutated?
void linear_unsafe_dense_matrix_add( dense_matrix* matrix1, dense_matrix* matrix2
			           , cholmod_common* chol );

void linear_unsafe_dense_vector_add( dense_vector* vector1, dense_vector* vector2
			           , cholmod_common* chol );

void linear_sparse_apply( sparse_matrix* matrix, dense_vector* vector
			, dense_vector* out_vector
			, cholmod_common* chol );

void linear_dense_apply( dense_matrix* matrix, dense_vector* vector
		       , dense_vector* out_vector
		       , cholmod_common* chol );

dense_matrix* linear_dense_transpose(dense_matrix *m, cholmod_common *chol);

void linear_unsafe_dense_transpose(dense_matrix *matrix, cholmod_common *chol);

// WARNING: Takes the _transposed_ covarate matrix!
void linear_dense_lse( dense_matrix* covariateT, dense_vector* response
		     , dense_matrix* out_hat
		     , cholmod_common* chol );

// WARNING: Takes the _transposed_ covarate matrix!
void linear_unsafe_dense_lse( dense_matrix* covariateT, dense_vector* response
			    , dense_matrix* out_hat
			    , cholmod_common* chol );

void linear_unsafe_dense_lse2( dense_matrix* covariateT, dense_vector* response
			    , dense_matrix* out_hat
			    , cholmod_common* chol );

double linear_dense_distance(dense_vector *a, dense_vector *b);
double linear_dense_distance2(dense_vector *a, dense_vector *b); //distance ^ 2

typedef double (*distribution)(const void* args, const gsl_rng* gen);

dense_matrix* linear_dense_matrix_random( const unsigned int n, const unsigned int m
					, const gsl_rng* gen, distribution dist, const void* args
					, cholmod_common* chol );

sparse_matrix* linear_sparse_matrix_random( const unsigned int n, const unsigned int m
					  , const gsl_rng* gen, distribution dist, const void* args
					  , cholmod_common* chol );

dense_matrix* linear_dense_vector_random( const unsigned int n
					, const gsl_rng* gen, distribution dist, const void* args
					, cholmod_common* chol );

void linear_dump_dense_matrix(dense_matrix* matrix, cholmod_common* chol);
void linear_dump_dense_vector(dense_vector* vector, cholmod_common* chol);
void linear_dump_sparse_matrix(sparse_matrix* matrix, cholmod_common* chol);

typedef dense_vector dense_matrix_column;
typedef dense_vector dense_matrix_row;

dense_matrix_column* linear_dense_matrix_columns( dense_matrix* matrix
						, cholmod_common* chol );

dense_matrix_row* linear_dense_matrix_rows( dense_matrix* matrix
					  , cholmod_common* chol );

void linear_dense_matrix_columns_free( dense_matrix_column* column
				     , cholmod_common* chol );

void linear_dense_matrix_rows_free( dense_matrix_row* rows
				  , cholmod_common* chol );

void linear_next_column( dense_matrix_column* column
		       , cholmod_common* chol );

void linear_next_row( dense_matrix_row* row
		    , cholmod_common* chol );

void linear_forward_columns_n( const int n
			     , dense_matrix_column* column
			     , cholmod_common* chol );

typedef dense_matrix dense_sub_matrix;

dense_sub_matrix* linear_dense_matrix_slice_columns( const unsigned int from
						   , const unsigned int to
						   , dense_matrix* matrix
						   , cholmod_common* chol );

dense_sub_matrix* linear_dense_matrix_slice_rows( const unsigned int from
						, const unsigned int to
						, dense_matrix* matrix
						, cholmod_common* chol );

void linear_dense_sub_matrix_free( dense_sub_matrix* submatrix
				 , cholmod_common* chol );

#endif
