#include "linear.h"
#include "echo.h"
#include "echo_util.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

echo_dynamics* prune( dense_matrix* matrix
		    , double support
		    , double epsilon
		    , cholmod_common* chol ) {
    dense_matrix* matrix_backup = cholmod_copy_dense(matrix, chol);

    double scale = support / (support - epsilon);

    const unsigned int range = matrix->nzmax;
    double* ptr = (double*) matrix_backup->x;
    for(int i = 0; i < range; ++i) {
	const double sign  = (ptr[i] > 0) - (ptr[i] < 0);
	const double nzero = (fabs(ptr[i]) > epsilon);
	ptr[i] = scale * sign * nzero * (fabs(ptr[i]) - epsilon);
    }
    
    echo_dynamics* result = linear_dense_to_sparse(matrix_backup, chol);
    linear_dense_matrix_free(matrix_backup, chol);
    
    return result;
}

// TODO: Change this to take a distribution and add it to util.
void perturb_additative_dense( dense_matrix* matrix
			     , double sd, gsl_rng* gen
			     , cholmod_common* chol ) {
    const unsigned int range = matrix->nzmax;
    double* ptr = (double*)matrix->x;

    for(int i = 0; i < range; ++i){
	ptr[i] += gsl_ran_gaussian(gen, sd);;
    }
}

void perturb_additative_sparse( sparse_matrix* matrix
			      , double sd, gsl_rng* gen
			      , cholmod_common* chol ) {
    const unsigned int range = matrix->nzmax;
    double* ptr = (double*)matrix->x;

    for(int i = 0; i < range; ++i)
	ptr[i] += gsl_ran_gaussian(gen, sd);
}

void ensure_esp_sparse( sparse_matrix* matrix
		      , double spec
		      , gsl_rng* gen
		      , cholmod_common* chol ) {
    dense_vector* eigenvector =
	linear_dense_vector_random(matrix->nrow, gen, &dense_dist, (void*)0, chol);

    double eigenvalue = 
	power_iteration_sparse(matrix, eigenvector, chol);

    if (eigenvalue > 1){
	linear_unsafe_sparse_matrix_scale(spec/eigenvalue, matrix, chol);
    }

    linear_dense_vector_free(eigenvector, chol);
}

echo_network copy_network(echo_network net, cholmod_common* chol) {
    echo_network result;

    result.dynamics = cholmod_copy_sparse(net.dynamics, chol);
    result.readin   = cholmod_copy_dense(net.readin, chol);
    result.readout  = cholmod_copy_dense(net.readout, chol);

    return result;
}

int main(int argc, char** argv) {
    if(argc < 8) {
	printf("Usage: %s input target test_input test_target validation_input validation_target reservoir_size (lag) (replicates)\n", argv[0]);
	exit(EXIT_FAILURE);
    }

    cholmod_common chol;
    cholmod_start(&chol);

    gsl_rng *gen = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set (gen, time(NULL));

    const unsigned int reservoir_size = atoi(argv[7]);

    const unsigned int input_dim  = 1;
    const unsigned int output_dim = 1;

    const unsigned int lag = 
	argc < 9 ? 10 : atoi(argv[8]);

    const unsigned int replicates = 
	argc < 10 ? 100 : atoi(argv[9]);

    FILE* f_input = fopen(argv[1], "r");    
    if(!f_input) {
	printf("Unable to read input file: %s\n", argv[1]);
	exit(EXIT_FAILURE);
    }
    echo_series* input = 
	echo_util_read_doubles(f_input, &chol);
    fclose(f_input);

    FILE* f_target = fopen(argv[2], "r");    
    if(!f_target) {
	printf("Unable to read target file: %s\n", argv[2]);
	exit(EXIT_FAILURE);
    }
    echo_series* target =
	echo_util_read_doubles_lag(f_target, lag, &chol);
    fclose(f_target);

    FILE* f_test_input = fopen(argv[3], "r");    
    if(!f_test_input) {
	printf("Unable to read test_input file: %s\n", argv[3]);
	exit(EXIT_FAILURE);
    }
    echo_series* test_input =
	echo_util_read_doubles(f_test_input, &chol);
    fclose(f_test_input);

    FILE* f_test_target = fopen(argv[4], "r");    
    if(!f_test_target) {
	printf("Unable to read test_target file: %s\n", argv[4]);
	exit(EXIT_FAILURE);
    }
    echo_series* test_target =
	echo_util_read_doubles_lag(f_test_target, lag, &chol);
    fclose(f_test_target);

    FILE* f_valid_input = fopen(argv[5], "r");    
    if(!f_valid_input) {
	printf("Unable to read valid_input file: %s\n", argv[3]);
	exit(EXIT_FAILURE);
    }
    echo_series* valid_input =
	echo_util_read_doubles(f_valid_input, &chol);
    fclose(f_valid_input);

    FILE* f_valid_target = fopen(argv[6], "r");    
    if(!f_valid_target) {
	printf("Unable to read valid_target file: %s\n", argv[4]);
	exit(EXIT_FAILURE);
    }
    echo_series* valid_target =
	echo_util_read_doubles_lag(f_valid_target, lag, &chol);
    fclose(f_valid_target);

    double* train_mse = malloc(sizeof(double)*replicates);
    double* test_mse  = malloc(sizeof(double)*replicates);
    double* valid_mse  = malloc(sizeof(double)*replicates);

    echo_network net = 
	echo_util_make_network( reservoir_size, input_dim, output_dim
			      , gen, &chol );

    dense_matrix* dense_dynamics = 
	linear_dense_matrix_alloc(reservoir_size, reservoir_size, &chol);
    dense_matrix* dense_dynamics_back = 
	linear_sparse_to_dense(net.dynamics, &chol);

    /* echo_series *target =  */
    /* 	echo_util_output_series_for(net, target_data->nrow - lag, &chol); */
    /* memcpy( target->x */
    /* 	  , ((double*)target_data->x) + lag */
    /* 	  , (target->nrow)*sizeof(double) ); */

    /* echo_series *test_target =  */
    /* 	echo_util_output_series_for(net, test_target_data->nrow - lag, &chol); */
    /* memcpy( test_target->x */
    /* 	  , ((double*)test_target_data->x) + lag */
    /* 	  , (test_target->nrow)*sizeof(double) ); */

    /* echo_series *valid_target =  */
    /* 	echo_util_output_series_for(net, valid_target_data->nrow - lag, &chol); */
    /* memcpy( valid_target->x */
    /* 	  , ((double*)valid_target_data->x) + lag */
    /* 	  , (valid_target->nrow)*sizeof(double) ); */

    double mse;
    double mse_back;

    double residual;
    double* ptr_run;
    double* ptr_target;
    echo_series* run;
    const unsigned int run_length = test_target->nrow;

    // TODO: Something more systematic plz :P Right now this is just a
    // really large value X-D
#ifdef INFINITY
    mse_back = INFINITY;
#else
    mse_back = 10000000000000;
#endif

    time_t* at_times = malloc(sizeof(time_t)*replicates);
    //TODO: Should probably use clock_gettime, but somehow I'm too dumb
    time_t base_time = clock();

    const double sd_min = 0.0001;
    const unsigned int iter_max = 100;

    double sd = (1/0.9)*0.07;
    double inner_sd = sd;

    //    double* spectral_radii = malloc(sizeof(double)*replicates);
    //    double radius = 0.95;
    //    double radius_inner = radius;

    //    double* connectedness  = malloc(sizeof(double)*replicates);
    //    double connect = 0.2;
    //    double connect_inner = connect;

    for(int i = 0; i < replicates; ++i) {
	fprintf(stderr, "\n");

	if(sd > sd_min)
	    sd = 0.999*sd;
	
	inner_sd = (1/0.95)*sd;

	for( unsigned int iter_counter = 0
	   ; iter_counter < iter_max
	   ; ++iter_counter ) {
	    inner_sd = 0.95*inner_sd;
	    //	    radius_inner = radius + gsl_ran_gaussian(gen, 0.1);
	    //	    connect_inner = connect_inner + gsl_ran_gaussian(gen, 0.05);

	    linear_dense_matrix_copy_to(dense_dynamics, dense_dynamics_back, 1, &chol);
	    perturb_additative_dense(dense_dynamics, inner_sd, gen, &chol);
	    
	    linear_sparse_matrix_free(net.dynamics, &chol);
	    net.dynamics = prune(dense_dynamics, 1, 0.8, &chol);

	    // NOTE: Without this it doesn't work very well...
	    //  ensure_esp_sparse(net.dynamics, radius_inner, gen, &chol);
	    //	    perturb_additative_dense(net.readin, inner_sd, gen, &chol);
	    
	    echo_util_train_network(net, lag, input, target, gen, &chol);

	    run = echo_util_eval_network_all(net, lag, test_input, gen, &chol);

	    mse = 0;
	    ptr_run    = (double*)run->x;
	    ptr_target = (double*)test_target->x;
	    for(int j = 0; j < run_length; ++j) {
		residual = ptr_run[j] - ptr_target[j];
		mse += residual*residual;
	    } 
	    mse = mse / run_length;

	    echo_util_series_free(run, &chol);
	    
	    fprintf(stderr, "%f ", mse);
	    double relative = mse_back/mse;
	    if(mse < mse_back || gsl_rng_uniform(gen) < pow(relative, 5))
		break;
	}

	dense_matrix* swap = dense_dynamics;
	dense_dynamics = dense_dynamics_back;
	dense_dynamics_back = swap;

	//	radius = radius_inner;
	//	spectral_radii[i] = radius;

	//	connect = connect_inner;
	//	connectedness[i] = connect;

	mse_back = mse;
	test_mse[i] = mse;

	//TODO: Write an unsafe version, so that we can reuse run!
	run = echo_util_eval_network_all(net, lag, input, gen, &chol);

	mse = 0;
	ptr_run    = (double*)run->x;
	ptr_target = (double*)target->x;
	for(int j = 0; j < target -> nrow; ++j) {
	    residual = ptr_run[j] - ptr_target[j];
	    mse += residual*residual;
	} 
	train_mse[i] = mse/(target->nrow);

	echo_util_series_free(run, &chol);

	run = echo_util_eval_network_all(net, lag, valid_input, gen, &chol);

	mse = 0;
	ptr_run    = (double*)run->x;
	ptr_target = (double*)valid_target->x;
	for(int j = 0; j < valid_target -> nrow; ++j) {
	    residual = ptr_run[j] - ptr_target[j];
	    mse += residual*residual;
	} 
	valid_mse[i] = mse/(valid_target->nrow);

	echo_util_series_free(run, &chol);

	at_times[i] = clock() - base_time;
    }

    printf("\nValidation.MSE\tTest.MSE\tTrain.MSE\tTime\n");
    for(int i = 0; i < replicates; ++i)
	printf( "%f\t%f\t%f\t%Lf\t\n"
	      , valid_mse[i]
	      , test_mse[i]
	      , train_mse[i]
		// Lol, I wanted to be absolutely sure...
	      , ((long double)at_times[i])/((long double)CLOCKS_PER_SEC) );

    //    free(spectral_radii);
    //    free(connectedness);
    free(train_mse);
    free(test_mse);

    echo_util_series_free(input, &chol);
    echo_util_series_free(target, &chol);
    echo_util_series_free(test_input, &chol);
    echo_util_series_free(test_target, &chol);
    echo_util_series_free(valid_input, &chol);
    echo_util_series_free(valid_target, &chol);

    linear_dense_matrix_free(dense_dynamics, &chol);
    linear_dense_matrix_free(dense_dynamics_back, &chol);

    // FREE NETWORK!!

    cholmod_finish(&chol);

    gsl_rng_free(gen);

    return EXIT_SUCCESS;
}
