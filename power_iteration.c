#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <time.h>

#include <gsl/gsl_rng.h>

//#include <openblas/cblas.h>

#include <cholmod.h>

cholmod_dense* random_dense( const int nrow, const int ncol, const double fill
                           , const gsl_rng* gen, cholmod_common* chol) {
    cholmod_dense* result = cholmod_allocate_dense(nrow, ncol, nrow, CHOLMOD_REAL, chol);

    int range = result->nzmax;
    double* x = (double*)result->x;
    double tmp;
    double twiddle = 1/fill;
    for(int i = 0; i < range; ++i) {
	tmp  = gsl_rng_uniform(gen);
	x[i] = (tmp < fill)*twiddle*tmp;
    }

    return result;
}

cholmod_sparse* random_sparse( const int nrow, const int ncol, const double fill
                             , const gsl_rng* gen, cholmod_common* chol) {
    cholmod_dense* tmp = random_dense(nrow, ncol, fill, gen, chol);
    cholmod_sparse* result = cholmod_dense_to_sparse(tmp, 1, chol);
    cholmod_free_dense(&tmp, chol);
    return result;
}

void smult( cholmod_sparse* matrix \
          , cholmod_dense* in, cholmod_dense* out \
          , cholmod_common* chol ) {
    cholmod_sdmult( matrix, 0, (double[2]){1, 1}, (double[2]){0, 0}
                  , in, out, chol );
}

double norm(cholmod_dense* vec, cholmod_common* chol) {
    return cholmod_norm_dense(vec, 2, chol);
}

double normalise(cholmod_dense* vec, cholmod_common* chol) {
    double factor = norm(vec, chol);

    double* x = vec->x;
    int range = vec->nzmax;
    for(int i = 0; i < range; ++i) {
	x[i] /= factor;
    }

    return factor;
}

void swap(void** a, void** b) {
    // Non-clever version, in case a == b or I get things wrong...
    void* tmp = *a;
    *a = *b;
    *b = tmp;
}

void swap_dense(cholmod_dense* a, cholmod_dense* b) {
    swap(&(a->x), &(b->x));
}

void fbumb(double* a) {
    // TODO: We need an int type that's the size of a double, I don't
    // think this is platform agnostic...
    long long int* twiddler = (long long int*)a;
    *twiddler += 1;
}

//TODO: Add some validation (eigen being column vector e.t.c.)
double power_iteration_sparse( cholmod_sparse* matrix, cholmod_dense* eigen
                             , cholmod_common* chol ) {
    const double epsilon = 0.000001;

    double spec;
    double spec_back;

    normalise(eigen, chol);
    cholmod_dense* eigen_back = 
	cholmod_allocate_dense(eigen->nzmax, 1, eigen->nzmax, CHOLMOD_REAL, chol);

    smult(matrix, eigen, eigen_back, chol);
    spec = normalise(eigen_back, chol);
    swap_dense(eigen, eigen_back);

    // Make sure spec_back is strictly greater than spec + epsilon to
    // avoid early termination of the loop
    spec_back = spec + epsilon;
    fbumb(&spec_back);

    while(fabs(spec - spec_back) > epsilon) {
	spec_back = spec;

	smult(matrix, eigen, eigen_back, chol);
	spec = normalise(eigen_back, chol);
	swap_dense(eigen, eigen_back);
    }

    return spec;
}

int main(int argc, char** argv) {
    const int size = 300;

    cholmod_common chol;
    cholmod_start(&chol);
    chol.print = 4;

    gsl_rng *gen = gsl_rng_alloc(gsl_rng_mt19937);

    cholmod_sparse* mat = random_sparse(size, size, 0.25, gen, &chol);
    cholmod_dense* eigen = cholmod_ones(size, 1, CHOLMOD_REAL, &chol);

    double spec = power_iteration_sparse(mat, eigen, &chol);
    printf("Spectral radius %f\n", spec);

    cholmod_dense* eigen2 = cholmod_ones(size, 1, CHOLMOD_REAL, &chol);
    smult(mat, eigen, eigen2, &chol);

    cholmod_print_dense( eigen, "Before", &chol);
    cholmod_print_dense(eigen2, "After", &chol);

    cholmod_free_dense(&eigen, &chol);
    cholmod_free_sparse(&mat, &chol);

    gsl_rng_free(gen);

    cholmod_finish(&chol);

    return EXIT_SUCCESS;
}
