#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <time.h>

#include <gsl/gsl_rng.h>

#include <clapack.h>

double* random_dense( const int nrow, const int ncol, const double fill
                    , const gsl_rng* gen) {
    double* result = malloc(nrow * ncol * sizeof(double));

    int range = nrow*ncol;
    double tmp;
    double twiddle = 1/fill;
    for(int i = 0; i < range; ++i) {
	tmp  = gsl_rng_uniform(gen);
	result[i] = (tmp < fill)*twiddle*tmp;
    }

    return result;
}

int main(int argc, char** argv) {
    const int nrow = 400;
    const int ncol = 400;
    const int replicates = 100;

    gsl_rng *gen = gsl_rng_alloc(gsl_rng_mt19937);

    double* predictors = random_dense(nrow, ncol, 1, gen);
    double* targets    = random_dense(nrow,    1, 1, gen);
 
    time_t total = 0;
    time_t start;

    clapack_dgels( CblasColMajor, CblasNoTrans
                 , nrow, ncol, 1
                 , predictors, nrow
                 , targets, nrow );
    for(int i = 0; i < replicates; ++i) {
        start = clock();
        clapack_dgels( CblasColMajor, CblasNoTrans
                     , nrow, ncol/2, 1
                     , predictors, nrow
                     , targets, nrow );
        total += clock() - start;
    }

    printf("Regression: %f seconds\n", ((double)total)/((double)CLOCKS_PER_SEC*replicates));

    free(predictors);
    free(targets);

    gsl_rng_free(gen);

    return EXIT_SUCCESS;
}
