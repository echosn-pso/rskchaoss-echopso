#include "linear.h"
#include "echo.h"
#include "echo_util.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

int main(int argc, char** argv) {
    if(argc < 5) {
	printf("Usage: %s input target output reservoir_size (lag)\n", argv[0]);
	exit(EXIT_FAILURE);
    }

    cholmod_common chol;
    cholmod_start(&chol);

    gsl_rng *gen = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set (gen, time(NULL));

    const unsigned int reservoir_size = atoi(argv[4]);

    const unsigned int input_dim  = 1;
    const unsigned int output_dim = 1;

    const unsigned int lag = 
	argc == 5 ? 5 : atoi(argv[5]);

    FILE* f_input = fopen(argv[1], "r");    
    if(!f_input) {
	printf("Unable to read input file: %s\n", argv[1]);
	exit(EXIT_FAILURE);
    }
    echo_series* input = 
	echo_util_read_doubles(f_input, &chol);
    fclose(f_input);

    FILE* f_target = fopen(argv[2], "r");    
    if(!f_target) {
	printf("Unable to read target file: %s\n", argv[2]);
	exit(EXIT_FAILURE);
    }
    echo_series* target =
	echo_util_read_doubles_lag(f_target, lag, &chol);
    fclose(f_target);

    //NOTE: Superfluous allocation, at least only done once :P
    echo_network net = 
	echo_util_make_network( reservoir_size, input_dim, output_dim
			      , gen, &chol );

    dense_matrix* circ_dynamics = 
	linear_dense_matrix_alloc(reservoir_size, reservoir_size, &chol);

    const double const_value = 0.8;
    double* dyn_ptr = (double*)circ_dynamics->x;
    for(unsigned int col = 0; col < reservoir_size-1; ++col)
	dyn_ptr[col*reservoir_size] = 0;
    dyn_ptr[reservoir_size - 1] = const_value;

    for(unsigned int row = 1; row < reservoir_size; ++row) 
    for(unsigned int col = 0; col < reservoir_size; ++col) {
	const unsigned int idx = col*reservoir_size + row;
	dyn_ptr[idx] = (row == col+1) * const_value;
    }

    linear_sparse_matrix_free(net.dynamics, &chol);
    net.dynamics = linear_dense_to_sparse(circ_dynamics, &chol);
    linear_dense_matrix_free(circ_dynamics, &chol);

    echo_util_train_network(net, lag, input, target, gen, &chol);

    // NOTE: For debug :P Remove this
    echo_series* test_run = 
	echo_util_eval_network_all(net, lag, input, gen, &chol);

    FILE* f_network = fopen(argv[3], "w");
    echo_util_serialise_network(net, f_network, &chol);
    fclose(f_network);

    echo_util_net_free(net, &chol);
    echo_util_series_free(input, &chol);
    echo_util_series_free(target, &chol);
    echo_util_series_free(test_run, &chol);

    cholmod_finish(&chol);

    gsl_rng_free(gen);

    return EXIT_SUCCESS;
}
