#include "echo_util.h"
#include <gsl/gsl_randist.h>
#include "math.h"

const double support[10] = { 0, 0, 0, 0, 0.51, 1.10 };
double sparse_dist(const void* null, const gsl_rng* gen) {
    return support[gsl_rng_uniform_int(gen, 6)];
}

double sparse_dist_nondumb(const void* param, const gsl_rng* gen) {
    #ifndef NDEBUG
    if(param == 0) {
	fprintf(stderr, "sparse_dist: missing argument!\n");
	exit(EXIT_FAILURE);
    }
    #endif
    
    const double prob_zero = 1 - ((double*)param)[0];
    const double dist_max  = ((double*)param)[1];

    const double obs = gsl_rng_uniform(gen);

    return (obs > prob_zero) * dist_max * obs;
}

double dense_dist(const void* null, const gsl_rng* gen) {
    return gsl_ran_gaussian(gen, 1);
}

double trimmed_dist(const void *param, const gsl_rng *gen){
  const double *r = param ? param : (double[3]){.2, .2, 4};
  double x = gsl_ran_gaussian(gen, r[0]);
  return (x > r[1]) * (x - r[1]) * r[2];
}

// TODO: I think this is available in cholmod!
double norm(cholmod_dense* vec, cholmod_common* chol) {
    return cholmod_norm_dense(vec, 2, chol);
}

double normalise(cholmod_dense* vec, cholmod_common* chol) {
    double factor = norm(vec, chol);
    double c = 1/factor;
    double* x = vec->x;
    int range = vec->nzmax;
    for(int i = 0; i < range; ++i) {
	x[i] *= c;
    }

    return factor;
}

void swap(void** a, void** b) {
    // Non-clever version, in case a == b or I get things wrong...
    void* tmp = *a;
    *a = *b;
    *b = tmp;
}

void swap_dense(cholmod_dense* a, cholmod_dense* b) {
    swap(&(a->x), &(b->x));
}

void fbumb(double* a) {
    // TODO: We need an int type that's the size of a double, I don't
    // think this is platform agnostic...
    long long int* twiddler = (long long int*)a;
    *twiddler += 1;
}

void unsafe_twiddle( dense_matrix* dense_dynamics
		     , gsl_rng* gen
		     , cholmod_common* chol ) {
    static const double pm[2] = { -1.0, 1.0 };

    const unsigned int range = dense_dynamics->nzmax;
    double* x = (double*)dense_dynamics->x;

    for(int i = 0; i < range; ++i) {
	x[i] *= pm[gsl_rng_uniform_int(gen, 2)];
    }
}

//TODO: Add some validation (eigen being column vector e.t.c.)
double power_iteration_dense( cholmod_dense* matrix, cholmod_dense* eigen
			      , cholmod_common* chol ) {
    const double epsilon = 0.000001;

    double spec;
    double spec_back;
    int c = 0;

    normalise(eigen, chol);
    cholmod_dense* eigen_back = 
	cholmod_allocate_dense(eigen->nzmax, 1, eigen->nzmax, CHOLMOD_REAL, chol);
    void *orig_back = eigen_back -> x;
    void *orig_p = eigen -> x;

    linear_dense_apply(matrix, eigen, eigen_back, chol);
    spec = normalise(eigen_back, chol);
    swap_dense(eigen, eigen_back);

    // Make sure spec_back is strictly greater than spec + epsilon to
    // avoid early termination of the loop
    spec_back = spec + epsilon;
    fbumb(&spec_back);

    while(c++ < 100 && fabs(spec - spec_back) > epsilon) {
	spec_back = spec;

	linear_dense_apply(matrix, eigen, eigen_back, chol);
	spec = normalise(eigen_back, chol);
	swap_dense(eigen, eigen_back);
    }

    eigen -> x = orig_p;
    eigen_back -> x = orig_back;

    linear_dense_vector_free(eigen_back, chol);

    return spec;
}

double power_iteration_sparse( cholmod_sparse* matrix, cholmod_dense* eigen
			       , cholmod_common* chol ) {
    const double epsilon = 0.000001;

    double spec;
    double spec_back;
    int c = 0;

    normalise(eigen, chol);
    cholmod_dense* eigen_back = 
	cholmod_allocate_dense(eigen->nzmax, 1, eigen->nzmax, CHOLMOD_REAL, chol);
    void *orig_back = eigen_back -> x;
    void *orig_p = eigen -> x;

    linear_sparse_apply(matrix, eigen, eigen_back, chol);
    spec = normalise(eigen_back, chol);
    swap_dense(eigen, eigen_back);

    // Make sure spec_back is strictly greater than spec + epsilon to
    // avoid early termination of the loop
    spec_back = spec + epsilon;
    fbumb(&spec_back);

    while(c++ < 100 && fabs(spec - spec_back) > epsilon) {
	spec_back = spec;

	linear_sparse_apply(matrix, eigen, eigen_back, chol);
	spec = normalise(eigen_back, chol);
	swap_dense(eigen, eigen_back);
    }

    eigen -> x = orig_p;
    eigen_back -> x = orig_back;

    linear_dense_vector_free(eigen_back, chol);

    return spec;
}

echo_series* echo_util_series_alloc( const unsigned int entries 
				     , const unsigned int dimension
				     , cholmod_common* chol ) {
    return linear_dense_matrix_alloc(entries, dimension, chol);
}

echo_series* echo_util_output_series_for( const echo_network net
					  , const unsigned int entries 
					  , cholmod_common* chol ) {
    return linear_dense_matrix_alloc(entries, net.readout->nrow, chol);
}

echo_series* echo_util_input_series_for( const echo_network net
					 , const unsigned int entries 
					 , cholmod_common* chol ) {
    return linear_dense_matrix_alloc(entries, net.readin->ncol, chol);
}

void echo_util_series_free(echo_series* series, cholmod_common* chol) {
    linear_dense_matrix_free(series, chol);
}


echo_network echo_util_make_network( const unsigned int reservoir_size
				     , const unsigned int input_dim
				     , const unsigned int output_dim
				     , gsl_rng* gen
				     , cholmod_common* chol ) {
    echo_network result;

    result.dynamics = echo_util_generate_es_dynamics(reservoir_size, .95, gen, chol);
    /* result.dynamics = echo_util_generate_trimmed_dynamics(reservoir_size, gen, chol); */
    result.readin   = linear_dense_matrix_random( reservoir_size
						, input_dim , gen
						, &dense_dist, (const void*)0
						, chol );
    result.readout = linear_dense_matrix_alloc( output_dim
					      , reservoir_size + input_dim + 1
					      , chol );

    return result;
}

void echo_util_net_free(echo_network net, cholmod_common* chol) {
    linear_sparse_matrix_free(net.dynamics, chol);
    linear_dense_matrix_free(net.readin, chol);
    linear_dense_matrix_free(net.readout, chol);
}

sparse_matrix* echo_util_generate_trimmed_dynamics(int reservoir_size
						  , gsl_rng *gen
						  , cholmod_common *chol){
  dense_matrix *dense_dynamics = 
    linear_dense_matrix_random( reservoir_size, reservoir_size
			      , gen, trimmed_dist, (const void*)0
			      , chol );
  sparse_matrix *dynamics = linear_dense_to_sparse(dense_dynamics, chol);

  linear_dense_matrix_free(dense_dynamics, chol);
  return dynamics;
}

sparse_matrix* echo_util_generate_es_dynamics( int reservoir_size
					     , double srad
					     , gsl_rng *gen
					     , cholmod_common *chol ) {
    return echo_util_generate_es_dynamics_with( reservoir_size
					      , srad
					      , &sparse_dist
					      , (const void*)0
					      , gen
					      , chol);
}

sparse_matrix* echo_util_generate_es_dynamics_with( int reservoir_size
					          , double srad
						  , distribution dist
						  , const void* dist_param
					          , gsl_rng *gen
					          , cholmod_common *chol ) {
    dense_matrix* dense_dynamics = 
	linear_dense_matrix_random( reservoir_size, reservoir_size
				  , gen, dist, dist_param
				  , chol );

    dense_vector* eigenvector =
	linear_dense_vector_random(reservoir_size, gen, &dense_dist, (const void*)0, chol);

    double eigenvalue = 
	power_iteration_dense(dense_dynamics, eigenvector, chol);

    if (eigenvalue > 1){
	linear_unsafe_dense_matrix_scale(srad/eigenvalue, dense_dynamics, chol);
    }

    echo_dynamics* dynamics =
	linear_dense_to_sparse(dense_dynamics, chol);

    // TODO: Write a random for already allocated vectors
    // NOTE: Generate a new initial vector, just to be on the safe side
    linear_dense_vector_free(eigenvector, chol);
    eigenvector = 
	linear_dense_vector_random(reservoir_size, gen, &dense_dist, (void*)0, chol);

    eigenvalue = 
	power_iteration_sparse(dynamics, eigenvector, chol);

    linear_sparse_matrix_free(dynamics, chol);

    unsafe_twiddle(dense_dynamics, gen, chol);

    dynamics = linear_dense_to_sparse(dense_dynamics, chol);

    linear_dense_vector_free(eigenvector, chol);
    linear_dense_matrix_free(dense_dynamics, chol);

    // TODO: this is at least better than the old method (i.e. just
    // die), but could still be nicer...
    const double epsilon = 0.0001;
    if(fabs(eigenvalue - srad) < epsilon)
	return dynamics;
    else return (echo_dynamics*) 0;
}

echo_state* echo_util_make_state_for(echo_network net, gsl_rng* gen, cholmod_common* chol) {
    return linear_dense_vector_random(net.dynamics->nrow, gen, &dense_dist, (void*)0, chol);
}

void echo_util_state_free(echo_state* state, cholmod_common* chol) {
    linear_dense_vector_free(state, chol);
}


echo_series* echo_util_online_train( echo_network net
				     , echo_series* input
				     , echo_series* target
				     , echo_series* out_output
				     , double eta
				     , double decay
				     , gsl_rng* gen
				     , cholmod_common* chol){

    echo_dynamics* dynamics = net.dynamics;
    echo_readout*  readout  = net.readout;
    echo_readin*   readin   = net.readin;
    echo_state* initial = echo_util_make_state_for(net, gen, chol);

#ifndef NDEBUG
    // TODO: We need some additional validation here!
    check_valid_dynamics(    "echo_run_n", dynamics                         );
    check_compatible_state(  "echo_run_n", initial    , dynamics            );
    check_compatible_readin( "echo_run_n", readin     , input    , dynamics );
#endif

    const unsigned int reservoir_size  = dynamics->nrow;
    const unsigned int nreadout = reservoir_size + 1 + input->ncol;
    const unsigned int ninput = input->nrow;

    double delta;

    echo_series* error = echo_util_output_series_for(net, ninput, chol);
    dense_matrix_row* view_input  = linear_dense_matrix_rows(input, chol);
    dense_matrix_row* view_output  = linear_dense_matrix_rows(out_output, chol);
    dense_matrix_row* view_target  = linear_dense_matrix_rows(target, chol);
    dense_matrix_row* view_error  = linear_dense_matrix_rows(error, chol);

    // Temporary buffer for storing activation from input nodes
    echo_state* from_input = linear_dense_vector_alloc(reservoir_size, chol);

    echo_state* state_backbuf = linear_dense_vector_alloc(reservoir_size, chol);
    dense_vector* ext = linear_dense_vector_alloc(nreadout, chol);

    // Statically computed/allocated for use when applying the 
    // activation function
    double* state_iter_ptr;

    dense_sub_matrix* input_ext = linear_dense_matrix_slice_rows(reservoir_size + 1, reservoir_size + 1 + input->ncol, ext, chol);

    echo_state* swap;
    ((double*)ext->x)[reservoir_size] = 1;
          
    for(int i = 0; i < ninput; ++i) {
	linear_dense_vector_copy_to(ext, initial, 1, chol);
	linear_dense_vector_copy_to(input_ext, view_input, 0, chol);
      
	//update readout layer with delta rule
        linear_dense_apply(readout, ext, view_output, chol);
	delta = ((double*)view_target->x)[0] - ((double*)view_output->x)[0];
	((double*)view_error->x)[0] = delta;
	
	for (int j = 0; j < nreadout; j++){
	    ((double*)readout->x)[j] += delta * eta * ((double*)ext->x)[j];
	    //decay?
	    ((double*)readout->x)[j] *= decay;
	}

        linear_sparse_apply(dynamics, initial, state_backbuf, chol); //internal dynamics
        linear_dense_apply(readin, view_input, from_input, chol); //input dynamics
	linear_unsafe_dense_vector_add(state_backbuf, from_input, chol); //combine

	// Activation function is applied pointwise
	state_iter_ptr = (double*)state_backbuf->x;
	for(int l = 0; l < reservoir_size; ++l) {
	    state_iter_ptr[l] = ECHO_ACTIVATION(state_iter_ptr[l]);
	    /* fprintf(stdout,"%.2f, ", state_iter_ptr[l]); */
	}
	/* fprintf(stdout,"\nrun: "); */

	linear_next_row(view_target, chol);
	linear_next_row(view_input, chol);
	linear_next_row(view_output, chol);
	linear_next_row(view_error, chol);

	swap = initial;
	initial = state_backbuf;
	state_backbuf = swap;
    }

    linear_dense_vector_free(from_input, chol);
    linear_dense_vector_free(ext, chol);
    linear_dense_matrix_rows_free(view_target, chol);
    linear_dense_matrix_rows_free(view_input, chol);
    linear_dense_matrix_rows_free(view_output, chol);

    echo_util_state_free(initial, chol);

    return error;
}

void echo_util_train_network( echo_network net
			      , const unsigned int lag
			      , echo_series* input
			      , echo_series* target
			      , gsl_rng* gen
			      , cholmod_common* chol) {
    echo_state* initial = echo_util_make_state_for(net, gen, chol);
    
    echo_train_readout( lag
			, net.dynamics , initial
			, net.readin   , input
			, target
			, net.readout
			, chol );

    echo_util_state_free(initial, chol);
}

void check_sufficient_input( const char const * name
			     , const echo_series const * input
			     , const unsigned int num ) {
    if(input->nrow < num) {
	printf( "%s: Insufficient input, has %zu, requested %d.\n"
		, name, input->nrow, num );
	exit(EXIT_FAILURE);
    }
}

echo_series* echo_util_eval_network( echo_network net
				     , const unsigned int lag
				     , const unsigned int num
				     , echo_series* input
				     , gsl_rng* gen
				     , cholmod_common* chol) {
#ifndef NDEBUG
    // TODO: Maybe we should add an option to pad?
    check_sufficient_input("util_eval_network", input, num);
#endif
    echo_state* initial = echo_util_make_state_for(net, gen, chol);
    
    echo_series* result = echo_util_output_series_for(net, num - lag, chol);

    echo_run_n(num, lag, net, initial, input, result, chol);

    echo_util_state_free(initial, chol);

    return result;
}

echo_series* echo_util_eval_network_all( echo_network net
					 , const unsigned int lag
					 , echo_series* input
					 , gsl_rng* gen
					 , cholmod_common* chol) {
    return echo_util_eval_network(net, lag, input->nrow, input, gen, chol);
}

/* Predicts the STEPS * LOOKAHEAD steps continuation of the series INPUT. The network is expected to output the LOOKAHEAD step prediction of the input. */
echo_series* echo_util_piecewise_prediction( echo_network net
					   , const unsigned int steps
					   , const unsigned int lookahead
					   , echo_series* input
					   , gsl_rng* gen
					   , cholmod_common* chol ) {

    if (lookahead < 1 || input -> nrow - lookahead < 0){
	fprintf( stderr
	       , "piecewise predict: invalid dimensions (got %d lookahead, %zu data)\n"
	       , lookahead
	       , input->nrow);
	exit(EXIT_FAILURE);
    }
  
    echo_series *output = echo_util_output_series_for(net, steps * lookahead, chol);
    echo_state *initial = echo_util_make_state_for(net, gen, chol);
    echo_state *after_lag = echo_util_make_state_for(net, gen, chol);
    echo_series *input_buf = echo_util_input_series_for(net, lookahead, chol);
    echo_series *input_backbuf = echo_util_input_series_for(net, lookahead, chol);
    dense_sub_matrix *input_after_lag = 
	linear_dense_matrix_slice_rows( input->nrow - lookahead, input->nrow
				      , input, chol );
    dense_matrix *swap;
    int i;

    echo_push_n( input->nrow - lookahead
	       , net.dynamics
	       , initial
	       , net.readin
	       , input
	       , after_lag
	       , chol);

    swap = initial;
    initial = after_lag;
    after_lag = swap;

    linear_dense_vector_copy_to(after_lag, initial, 1, chol);

    echo_unsafe_run_n(lookahead, 0, net, initial, input_after_lag, input_buf, chol);
    /* linear_dense_vector_smooth(input_buf, chol); */

    /* // build smooth transition */
    /* for (i = 0; i < lookahead; i++){ */
    /* 	((double*)input_backbuf -> x)[i] = i / (double)lookahead * ((double*)input_buf -> x)[i]  */
    /* 	    + (lookahead - i) / (double)lookahead * ((double*)input_after_lag -> x)[i]; */
    /* } */

    /* swap = initial; */
    /* initial = after_lag; */
    /* after_lag = swap; */

    /* echo_unsafe_run_n(lookahead, 0, net, initial, input_backbuf, input_buf, chol); */
    
    for (i = 0; i < steps - 1; i++){
	linear_dense_vector_smooth(input_buf, chol);

	dense_sub_matrix *output_slice = 
	    linear_dense_matrix_slice_rows(i*lookahead, (i+1)*lookahead, output, chol);
	linear_dense_vector_copy_to(output_slice, input_buf, 1, chol);
	linear_dense_sub_matrix_free(output_slice, chol);

	echo_unsafe_run_n(lookahead, 0, net, initial, input_buf, input_backbuf, chol);
	
	swap = input_buf;
	input_buf = input_backbuf;
	input_backbuf = swap;
    }

    linear_dense_vector_smooth(input_buf, chol);
    dense_sub_matrix *output_slice = linear_dense_matrix_slice_rows(i*lookahead, (i+1)*lookahead, output, chol);
    linear_dense_vector_copy_to(output_slice, input_buf, 1, chol);
    linear_dense_sub_matrix_free(output_slice, chol);

    echo_util_state_free(initial, chol);
    echo_util_state_free(after_lag, chol);
    echo_util_series_free(input_buf, chol);
    echo_util_series_free(input_backbuf, chol);
    linear_dense_sub_matrix_free(input_after_lag, chol);
    
    return output;
}


typedef enum serialise_magic_e {
    e_dense,
    e_sparse,
    e_dynamics,
    e_readin,
    e_readout,
    e_network,
    max_magic_serialise
} serialise_magic;

const serialise_magic magic[max_magic_serialise] = 
    { e_dense
    , e_sparse
    , e_dynamics
    , e_readin
    , e_readout
    , e_network };

const char const * magic_names[max_magic_serialise] = 
    { "Dense"
    , "Sparse"
    , "Dynamics"
    , "Readin"
    , "Readout"
    , "Network" };

const char* decode_magic(serialise_magic magic) {
    if(0 <= magic && magic < max_magic_serialise)
	return magic_names[magic];
    else
	return "Unknown"; 
}

void echo_util_serialise_dense_matrix(dense_matrix* matrix, FILE* stream, cholmod_common* chol) {
    fwrite(magic + e_dense, sizeof(serialise_magic), 1, stream);

    if(fwrite((void*)matrix, sizeof(dense_matrix), 1, stream) < 1) {
	printf("util_serialise_dense_matrix: Failed write (struct).\n");
	exit(EXIT_FAILURE);
    }

    // TODO: Would this be nicer or just fiddlyer with a column iterator?
    const unsigned int cols = matrix->ncol;
    const unsigned int rows = matrix->nrow;
    double* data = (double*)matrix->x;
    for(int col = 0; col < cols; ++col) {
	if(fwrite(data, sizeof(double), rows, stream) < rows) {
	    printf("util_serialise_dense_matrix: Failed write (data).\n");
	    exit(EXIT_FAILURE);
	}
	data += matrix->d;
    }
}

void echo_util_serialise_dense_vector(dense_vector* vector, FILE* stream, cholmod_common* chol) {
    echo_util_serialise_dense_matrix(vector, stream, chol);
}

// TODO: We should be able to directly serialise, but I don't be
// bothered with making sure it'd work right now...
void echo_util_serialise_sparse_matrix(sparse_matrix* matrix, FILE* stream, cholmod_common* chol) {
    fwrite(magic + e_sparse, sizeof(serialise_magic), 1, stream);
    dense_matrix* dense = linear_sparse_to_dense(matrix, chol);
    echo_util_serialise_dense_matrix(dense, stream, chol);
    linear_dense_matrix_free(dense, chol);
}

void echo_util_serialise_dynamics(echo_dynamics* dynamics, FILE* stream, cholmod_common* chol) {
    fwrite(magic + e_dynamics, sizeof(serialise_magic), 1, stream);
    echo_util_serialise_sparse_matrix(dynamics, stream, chol);
}

void echo_util_serialise_readin(echo_readin* dynamics, FILE* stream, cholmod_common* chol) {
    fwrite(magic + e_readin, sizeof(serialise_magic), 1, stream);
    echo_util_serialise_dense_matrix(dynamics, stream, chol);
}

void echo_util_serialise_readout(echo_readout* dynamics, FILE* stream, cholmod_common* chol) {
    fwrite(magic + e_readout, sizeof(serialise_magic), 1, stream);
    echo_util_serialise_dense_matrix(dynamics, stream, chol);
}

void echo_util_serialise_network(echo_network net, FILE* stream, cholmod_common* chol) {
    fwrite(magic + e_network, sizeof(serialise_magic), 1, stream);
    echo_util_serialise_dynamics( net.dynamics , stream , chol );
    echo_util_serialise_readin(   net.readin   , stream , chol );
    echo_util_serialise_readout(  net.readout  , stream , chol );
}

void ensure_magic(serialise_magic expected, FILE* stream) {
    serialise_magic got = -1;
    fread(&got, sizeof(serialise_magic), 1, stream);

    if(got != expected) {
	printf( "Wrong magic number, got %s, expected %s\n"
		, decode_magic(got), decode_magic(expected) );
	exit(EXIT_FAILURE);
    }
}

dense_matrix*  echo_util_deserialise_dense_matrix(FILE* stream, cholmod_common* chol) {
    ensure_magic(e_dense, stream);

    dense_matrix* read = malloc(sizeof(dense_matrix));
    if(fread((void*) read, sizeof(dense_matrix), 1, stream) < 1) {
	printf("util_deserialise_dense_matrix: Failed read (struct).\n");
	exit(EXIT_FAILURE);
    }
    
    dense_matrix* result = linear_dense_matrix_alloc(read->nrow, read->ncol, chol);
    if(fread(result->x, sizeof(double), read->nzmax, stream) < read->nzmax) {
	printf("util_deserialise_dense_matrix: Failed read (data).\n");
	exit(EXIT_FAILURE);
    }

    free(read);

    return result;
}

dense_vector*  echo_util_deserialise_dense_vector(FILE* stream, cholmod_common* chol) {
    return echo_util_deserialise_dense_matrix(stream, chol);
}

sparse_matrix* echo_util_deserialise_sparse_matrix(FILE* stream, cholmod_common* chol) {
    ensure_magic(e_sparse, stream);
    dense_matrix* dense = echo_util_deserialise_dense_matrix(stream, chol);

    sparse_matrix* sparse = linear_dense_to_sparse(dense, chol);

    linear_dense_matrix_free(dense, chol);

    return sparse;
}

echo_dynamics* echo_util_deserialise_dynamics(FILE* stream, cholmod_common* chol) {
    ensure_magic(e_dynamics, stream);
    return echo_util_deserialise_sparse_matrix(stream, chol);
}

echo_readin* echo_util_deserialise_readin(FILE* stream, cholmod_common* chol) {
    ensure_magic(e_readin, stream);
    return echo_util_deserialise_dense_matrix(stream, chol);
}

echo_readout* echo_util_deserialise_readout(FILE* stream, cholmod_common* chol) {
    ensure_magic(e_readout, stream);
    return echo_util_deserialise_dense_matrix(stream, chol);
}

echo_network echo_util_deserialise_network(FILE* stream, cholmod_common* chol) {
    ensure_magic(e_network, stream);
    echo_network result;

    result.dynamics = echo_util_deserialise_dynamics( stream , chol );
    result.readin   = echo_util_deserialise_readin(   stream , chol );
    result.readout  = echo_util_deserialise_readout(  stream , chol );

    return result;
}

echo_series* echo_util_read_doubles(FILE* stream, cholmod_common* chol) {
    unsigned int line_count = 0;
    // Thanks Jerry Coffin at :
    // http://stackoverflow.com/questions/4278845/count-the-lines-of-a-file-in-c
    while (EOF != (fscanf(stream, "%*[^\n]"), fscanf(stream, "%*c")))
	++line_count;

    echo_series* buffer = echo_util_series_alloc(line_count, 1, chol); 

    fseek(stream, 0, SEEK_SET);
    double* end = ((double*)buffer->x) + line_count;
    for(double* iter = (double*)buffer->x; iter < end; ++iter)
	fscanf(stream, "%lf\n", iter);

    return buffer;
}

echo_series* echo_util_read_doubles_lag(FILE* stream, const unsigned int lag, cholmod_common* chol) {
    unsigned int line_count = 0;
    // Thanks Jerry Coffin at :
    // http://stackoverflow.com/questions/4278845/count-the-lines-of-a-file-in-c
    while (EOF != (fscanf(stream, "%*[^\n]"), fscanf(stream, "%*c")))
	++line_count;

    if (line_count <= lag){
	fprintf(stderr,"read_doubles: insufficient data! has %d, required %d.\n", line_count, lag + 1);
	exit(EXIT_FAILURE);
    }

    echo_series* buffer = echo_util_series_alloc(line_count - lag, 1, chol); 

    fseek(stream, 0, SEEK_SET);
    double* end = ((double*)buffer->x) + line_count - lag;
    double *iter = buffer -> x;
    int j;

    //skip lag lines
    for (j = 0; j < lag; j++)
	fscanf(stream, "%lf\n", iter);

    for(iter = (double*)buffer->x; iter < end; ++iter)
	fscanf(stream, "%lf\n", iter);

    return buffer;
}


void echo_util_write_doubles(FILE* stream, echo_series *data, cholmod_common* chol) {
    int i;
    for (i = 0; i < data -> nrow; i++){
	fprintf(stream, "%lf\n", ((double*)data->x)[i]);
    }
}
