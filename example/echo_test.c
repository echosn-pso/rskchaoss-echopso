#include "linear.h"
#include "echo.h"

#include <stdlib.h>
#include <stdio.h>

int main(int argc, char** argv) {
    cholmod_common chol;
    cholmod_start(&chol);

    // TODO: Change this once we have allocation routines for the echo
    // stuff!
    echo_state* initial = linear_dense_vector_alloc(2, &chol);
    ((double*)initial->x)[0] = 1;
    ((double*)initial->x)[1] = 0;

    dense_matrix* dense_dynamics = linear_dense_matrix_alloc(2, 2, &chol);
    ((double*)dense_dynamics->x)[0] = 0;
    ((double*)dense_dynamics->x)[1] = 1;
    ((double*)dense_dynamics->x)[2] = 1;
    ((double*)dense_dynamics->x)[3] = 0;
    echo_dynamics* dynamics = linear_dense_to_sparse(dense_dynamics, &chol);

    echo_series* trace = linear_dense_matrix_alloc(2, 5, &chol);
    echo_series* flip  = linear_dense_vector_alloc(2, &chol);

    // TODO: These have too few arguments.
    //    echo_steps_n(5, dynamics, initial, trace, &chol);
    
    //    echo_unsafe_push_n(7, dynamics, initial, flip, &chol);
    
    linear_dump_dense_matrix(trace, &chol);
    linear_dump_dense_vector(flip, &chol);

    linear_dense_vector_free(flip, &chol);
    linear_dense_matrix_free(trace, &chol);
    linear_dense_matrix_free(dense_dynamics, &chol);
    linear_sparse_matrix_free(dynamics, &chol);
    linear_dense_vector_free(initial, &chol);

    cholmod_finish(&chol);

    return EXIT_SUCCESS;
}
