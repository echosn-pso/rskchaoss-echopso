#include "linear.h"

#include <stdlib.h>
#include <stdio.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

double simple_discrete_dist(void* null, const gsl_rng* gen) {
    const static double values[4] = {-1.43, 0.12, 0.34, 2};
    return values[gsl_rng_uniform_int(gen, 4)];
}

double standard_normal_dist(void* null, const gsl_rng* gen) {
    return gsl_ran_gaussian(gen, 0.25);
}

// TODO: I think this isn't "really" well defined unless you use
// orthogonal polynomials, but this will have to do for some basic
// testing :P
dense_matrix* polynomial_model( const int degree
			      , dense_vector* points
			      , cholmod_common* chol) {
    const int size = points->nzmax;

    dense_matrix* model = linear_dense_matrix_alloc(size, degree+1, chol);

    double* x = (double*)model->x;
    double* pts = (double*)points->x;

    double* init = x;
    double* xprev = x;
    for(; x < init + size; ++x)
	*x = 1;

    int i = 0;
    while(x < init + (degree+1)*size) {
	*x = (*xprev)*pts[i];
	i = (i+1) % size;
	++x; ++xprev;
    }

    return model;
}

// TODO: Is the row major thing a problem?

int main(int argc, char** argv) {
    gsl_rng* gen = gsl_rng_alloc(gsl_rng_mt19937);

    cholmod_common chol;
    cholmod_start(&chol);

    dense_matrix* id = linear_dense_identity(5, 5, &chol);
    linear_unsafe_dense_matrix_scale( 2,   id, &chol);

    sparse_matrix* spid = linear_sparse_identity(5, 5, &chol);
    linear_unsafe_sparse_matrix_scale(0.5, spid, &chol);

    dense_vector* v  = 
	linear_dense_vector_random(5, gen, &simple_discrete_dist, (void*)0, &chol);
    dense_vector* result = linear_dense_vector_alloc(5, &chol);

    linear_dense_apply(id, v, result, &chol);
    linear_sparse_apply(spid, v, result, &chol);

    linear_dense_vector_free( result, &chol);
    linear_dense_vector_free(      v, &chol);
    linear_dense_matrix_free(     id, &chol);
    linear_sparse_matrix_free(  spid, &chol);

    /// REGRESSION

    dense_vector* x = linear_dense_vector_alloc(20, &chol);
    for(int i = 0; i < (x->nzmax)/2; ++i) {
	((double*)x->x)[  2*i  ] = i;
	((double*)x->x)[2*i + 1] = i;
    }

    dense_matrix* model = 
	polynomial_model(1, x, &chol);

    dense_vector* real = 
	linear_dense_vector_random(2, gen, &simple_discrete_dist, (void*)0, &chol);

    dense_vector* noise = 
	linear_dense_vector_random(20, gen, &standard_normal_dist, (void*)0, &chol);

    dense_vector* observation = linear_dense_vector_alloc(20, &chol);
    linear_dense_apply(model, real, observation, &chol);

    linear_unsafe_dense_vector_add(observation, noise, &chol);

    linear_dump_dense_vector( real, &chol);
    linear_dump_dense_matrix(model, &chol);
    linear_dump_dense_vector(observation, &chol);

    dense_vector* estimate = linear_dense_vector_alloc(2, &chol);
    linear_unsafe_dense_lse(model, observation, estimate, &chol);

    linear_dump_dense_vector(estimate, &chol);

    linear_dense_vector_free(          x, &chol);
    linear_dense_vector_free(       real, &chol);
    linear_dense_vector_free(      noise, &chol);
    linear_dense_vector_free(observation, &chol);
    linear_dense_vector_free(   estimate, &chol);
    linear_dense_matrix_free(      model, &chol);

    cholmod_finish(&chol);

    gsl_rng_free(gen);

    return EXIT_SUCCESS;
}
