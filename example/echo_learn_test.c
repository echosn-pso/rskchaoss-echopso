#include "linear.h"
#include "echo.h"
#include "echo_util.h"
#include "echo_series.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#define M_PI 3.141596

int main(int argc, char** argv) {
    cholmod_common chol;
    cholmod_start(&chol);

    gsl_rng *gen = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set (gen, time(NULL));
    // TODO: There's some issue when the reservoir is bigger than 
    // the number of smaples (some paramters wrong in some multiplication
    // somewhere). Figure this out...

    int reservoir_size = 20;
    const unsigned int lag = 100;
    const unsigned int shift = 6;
    const unsigned int input_size = 2000;
    const unsigned int ntest = 1;

    reservoir_size = atoi(argv[1]);

    int i;

    echo_series* ar_series = sim_ar(input_size + shift, 2, 0.2, (double[2]){0.72, -0.9}, gen, &chol);
    echo_series* ar_series2 = sim_ar(input_size + shift, 2, 0.2, (double[2]){0.72, -0.9}, gen, &chol);

    echo_dynamics* dynamics[ntest];

    for (i = 0; i < ntest; i++){
      //fprintf(stdout,"generating net %d...\n", i);
      dynamics[i] = echo_util_generate_es_dynamics(reservoir_size, .95, gen, &chol);
    }

    echo_network net = echo_util_make_network(reservoir_size, 1, 1, gen, &chol);
    linear_sparse_matrix_free(net.dynamics, &chol);

    echo_series* input = echo_util_input_series_for(net, input_size, &chol);
    memcpy(input->x, ar_series->x, sizeof(double)*input_size);

    echo_series* target = echo_util_output_series_for(net, input_size - lag, &chol);
    memcpy(target->x, (void*)(((double*)ar_series->x)+lag+shift), sizeof(double)*(input_size - lag));

    echo_series* input_test = echo_util_input_series_for(net, input_size, &chol);
    memcpy(input_test->x, ar_series2->x, sizeof(double)*input_size);

    echo_series* target_test = echo_util_output_series_for(net, input_size - lag, &chol);
    memcpy(target_test->x, (void*)(((double*)ar_series2->x)+lag+shift), sizeof(double)*(input_size - lag));

    int tmax = fmin(input_size, 300);
    echo_series* run_result;

    echo_state* initial  = echo_util_make_state_for(net, gen, &chol);

    for (i = 0; i < ntest; i++){

      //fprintf(stdout,"%d\t", i);
      net.dynamics = dynamics[i];

      echo_util_train_network(net, lag, input, target, gen, &chol);
      run_result = echo_util_eval_network(net, lag, tmax, input_test, gen, &chol);

      for(i = 0; i < tmax - lag; ++i) {
      	printf("%d\t%f\t"  , i , ((double*)run_result->x)[i]);
      	printf("%f\n" , ((double*)target_test->x)[i]);
      }

      int nr = target_test->nrow;
      target_test->nrow = run_result->nrow;
      fprintf(stderr,"%.3e\n", linear_dense_distance(run_result, target_test));
      target_test->nrow = nr;
      linear_dense_matrix_free(  run_result, &chol);
    }

    net.dynamics = NULL;

    echo_util_net_free(net, &chol);
    linear_dense_vector_free(  initial   , &chol);
    linear_dense_matrix_free(  input     , &chol);
    linear_dense_matrix_free(  target    , &chol);
    linear_dense_matrix_free(  input_test, &chol);
    linear_dense_matrix_free(  target_test,&chol);

    for (i = 0; i < ntest; i++){
      linear_sparse_matrix_free(dynamics[i], &chol);
    }

    linear_dense_vector_free(  ar_series , &chol);
    linear_dense_vector_free(  ar_series2, &chol);
    
    cholmod_finish(&chol);

    gsl_rng_free(gen);

    return EXIT_SUCCESS;
}
